function generarFibonacci(cuantosNumeros) {
    let x = 1;
    let y = 1;
    let secuencia = [x, y];

    Array(cuantosNumeros).fill().forEach(() => {
        let sum = x + y;
        secuencia.push(sum);
        x = y;
        y = sum;
    });

    return secuencia;
}